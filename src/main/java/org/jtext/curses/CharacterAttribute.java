package org.jtext.curses;

public enum CharacterAttribute {
    NORMAL,
    STANDOUT,
    UNDERLINE,
    REVERSE,
    BLINK,
    DIM,
    BOLD,
    INVIS,
    ITALIC
}
